<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
include 'koneksi.php';
session_start();
$username = $_SESSION['username'];
$cek_user = mysqli_query($koneksi, "SELECT * FROM admindnowner WHERE username = '$username' ");
$pecah = mysqli_fetch_assoc($cek_user);
?>


<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>STAMINA--Manage Admin</title>
    <!-- Bootstrap Styles-->

    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="fh5co-logo">
                    <a class="navbar-brand" href="manage_admin.php"><strong>STAMINA <span>.</span> </strong></a></div>
            </div>

            <ul class="nav navbar-top-links navbar-right">

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="manage_admin_userprofile.php"><i class="fa fa-user fa-fw"></i> <?php echo $pecah['username'] ?> profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout.php" onclick="return confirm('Ingin Logout?')"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>

                </li>

            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div id="sideNav" href=""><i class="fa fa-caret-right"></i></div>
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li>
                        <a href="manage_admin.php"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>

                    <li>
                        <a href="kotakmasuk.php"><i class="fa fa-envelope-o"></i>Kotak Masuk</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-refresh"></i>Update Data<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">Informasi Kesehatan</a>
                            </li>
                            <li>
                                <a href="#">Video Latihan</a>
                            </li>
                            <li>
                                <a href="#">Kelas</a>
                            </li>
                            <li>
                                <a href="#">promosi</a>
                            </li>
                            <li>
                                <a href="#">Keanggotaan</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="penjualan.php"><i class="fa fa-shopping-cart"></i> Penjualan</a>
                    </li>
                    <li>
                        <a class="active-menu" href="laporanadmin.php"><i class="fa fa-file-o"></i> Laporan </a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Hallo <?php echo $pecah['username'] ?>!
                        </h1>

                    </div>
                </div>


                <!-- /. ROW  -->

                <div class="row">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <h2>Laporan</h2>
                            <br>
                            <ol class="breadcrumb">
                                <li class="active"><a href="laporanadmin.php">Data Admin</a></li>
                                <li><a href="laporanadmin_datauser.php">Data Pengunjung</a></li>
                                <li><a href="#">Data Penjualan</a></li>
                                <li><a href="laporan_admin_tanggapan.php">Data Tanggapan</a></li>
                            </ol>
                            <h2 style="text-align: center;" class="animated-box">Data Pengunjung</h2><br>
                            <a href="cetak.php"><button type="button" class="btn btn-primary btn-md active" style="color: white;"> Kirim</button></a>
                            ||
                            <a href="cetakpengunjung.php"><button type="button" class="btn btn-success btn-md active" style="color: white;">Cetak</button></a>
                            || <a href="hapussemuadatauser.php" onclick="return confirm('Ingin hapus semua data?')"><button type="button" class="btn btn-danger btn-md active" style="color: white;"> Hapus Semua</button></a>
                            <form class="form-inline" action="cari_data_pengunjung.php" name="search" method="GET" style="float: right;">
                                <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search">
                                <button class="btn btn-primary my-2 my-sm-0" style="margin-right: 30px;" type="submit">Search</button>
                            </form>
                            <br>
                            <br>
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Nama Depan</th>
                                        <th>Nama Belakang</th>
                                        <th>Email</th>
                                        <th>No Telepon</th>
                                        <th>Username</th>
                                        <th>Pilihan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    include "koneksi.php";
                                    if (isset($_GET['search'])) {
                                        $kata_kunci = $_GET['search'];
                                        $cari = "SELECT * FROM datainput WHERE id  like '%" .$kata_kunci. "%' OR namadep like '%" .$kata_kunci. "%'";
                                    } else {
                                        $cari = "SELECT * FROM datainput  ORDER BY id ASC ";
                                    }
                                    $result = mysqli_query($koneksi, $cari);
                                    if (!$result) {
                                        die("Query Error : " . mysqli_error($koneksi) . " - " . mysqli_error($koneksi));
                                    }

                                    while ($row = mysqli_fetch_assoc($result)) {
                                    ?>
                                        <tr>
                                            <td><?php echo $row['id'] ?></td>
                                            <td><?php echo $row['namadep'] ?></td>
                                            <td><?php echo $row['namabel'] ?></td>
                                            <td><?php echo $row['email'] ?></td>
                                            <td><?php echo $row['notlp'] ?></td>
                                            <td><?php echo $row['username'] ?></td>
                                            <td><a href="manage_admin_edit.php?id=<?php echo $row['id'] ?>" style ="background-color : #D5E3EB" class="btn  btn-sm">Edit</a> | 
                                            <a href="kotak_masuk_delete_cari.php?id=<?php echo $row['id'] ?>"class = "btn btn-danger btn-sm"  onclick ="return confirm('Yakin Ingin menghapus data ini?')">Delete</a></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!-- /. ROW  -->
        <footer>
            <div class="text-center w-full">
                <small class="block"> copyright &copy; STAMINA GYM. All Rights reserved.</small>
            </div>
        </footer>
    </div>
    <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- Bootstrap Js -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>


    <script src="assets/js/easypiechart.js"></script>
    <script src="assets/js/easypiechart-data.js"></script>


    <!-- Custom Js -->
    <script src="assets/js/custom-scripts.js"></script>


</body>

</html>