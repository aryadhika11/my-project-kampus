<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>STAMINA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free HTML5 Website Template by freehtml5.co" />
    <meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
    <meta name="author" content="freehtml5.co" />

    <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FreeHTML5.co
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800" rel="stylesheet">

    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>

    <div class="fh5co-loader"></div>

    <div id="page">
        <nav class="fh5co-nav" role="navigation">
            <div class="top">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <p class="num">Call: <a href="https://web.whatsapp.com/">+62 5710607708</a></p>
                            <ul class="fh5co-social">
                                <li><a href="#"><i class="icon-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-instagram"></i></a></li>
                                <li><a href="#"><i class="icon-twitter"></i></a></li>
                                <li><a href="#"><i class="icon-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-2">
                            <div id="fh5co-logo"><a href="index.php">Stamina<span>.</span></a></div>
                        </div>
                        <div class="col-xs-10 text-right menu-1">
                            <ul>
                                <li><a href="index.php">Home</a></li>
                                <li><a href="keanggotaan.php">Keanggotaan</a></li>
                                <li><a href="aboutus.php">About Us</a></li>
                                <li class="has-dropdown">
                                    <a>Informasi</a>
                                    <ul class="dropdown">
                                        <li><a href="jenissuplemen.php">Jenis Suplement</a></li>
                                        <li><a href="informasigizi.php">Informasi gizi</a></li>
                                        <li><a href="diet.php">Diet</a></li>
                                        <li><a href="kontak.php">Kontak Kami</a></li>
                                    </ul>
                                </li>
                                <li class="has-dropdown">
                                    <a>Program</a>
                                    <ul class="dropdown">
                                        <li><a href="program.php">Kelas Yoga</a></li>
                                        <li><a href="bodypump">Kelas Body Pump</a></li>
                                        <li><a href="videogym.php">Video Gym</a></li>
                                    </ul>
                                </li>
                                <li><a href="promosi.php">Promosi</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </nav>

        <header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(images/about-us-bg.jpg);" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <div class="display-t">
                            <div class="display-tc animate-box" data-animate-effect="fadeIn">
                                <h1>Jenis Suplement</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>


        <div id="fh5co-blog" class="fh5co-bg-section">
            <div class="container">
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                        <h2>SUPLEMENT GYM</h2>
                    </div>
                </div>
                <div class="row row-bottom-padded-md">
                    <div class="col-lg-4 col-md-4">
                        <div class="fh5co-blog animate-box">
                            <a href="#"><img class="img-responsive" style="width : 1500px" style="height : 100px" src="images/5 2.png" alt=""></a>
                            <div class="blog-text">
                                <h3><a href="" #>Casein Protein</a></h3>
                                <span class="posted_on">Rp. 400.000</span>
                                <span class="comment"><a href="">300<i class="icon-speech-bubble"></i></a></span>
                                <p>Kasein mengandung
                                    banyak asam amino
                                    glutamin yang berperan
                                    dalam menjaga massa
                                    otot, memulihkan otot,
                                    dan meningkatkan
                                    imunitas tubuh.</p>
                                <a href="#" class="btn btn-primary">Beli</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="fh5co-blog animate-box">
                            <a href="#"><img class="img-responsive" src="images/23png.png" alt=""></a>
                            <div class="blog-text">
                                <h3><a href="" #>Suplemen Creatin</a></h3>
                                <span class="posted_on">Rp. 350.000</span>
                                <span class="comment"><a href="">100<i class="icon-speech-bubble"></i></a></span>
                                <p>Suplemen kreatin
                                    banyak dipakai orang
                                    karena dipercaya
                                    sanggup mempercepat
                                    peningkatan massa otot. Suplemen kreatin bagus
                                    untuk orang kurus sebab mampu meningkatkan
                                    berat badan dengan
                                    cepat.</p>
                                <a href="#" class="btn btn-primary">Beli</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="fh5co-blog animate-box">
                            <a href="#"><img class="img-responsive"  src="images/33.png" alt=""></a>
                            <div class="blog-text">
                                <h3><a href="" #>Suplemen Glutamin</a></h3>
                                <b>
                                    <h3 style="color:red">PROMO!!</h3>
                                </b>
                                <span  class="posted_on">Rp.450.000</span>
                                <span class="comment"><a href="">140<i class="icon-speech-bubble"></i></a></span>
                                <p>Meningkatkan performa
                                    latihan,
                                    mengurangi rasa lelah berlebih,
                                    meningkatkan
                                    kemampuan pemulihan
                                    otot, membantu proses
                                    pembakaran lemak.</p>
                                <a href="#" class="btn btn-primary">Beli</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row-bottom-padded-md">
                    <div class="col-lg-4 col-md-4">
                        <div class="fh5co-blog animate-box">
                            <a href="#"><img class="img-responsive" src="images/22.png" alt=""></a>
                            <div class="blog-text">
                                <h3><a href="" #>Suplemen Beef Protein</a></h3>
                                <span class="posted_on">Rp.900.000</span>
                                <span class="comment"><a href="">200<i class="icon-speech-bubble"></i></a></span>
                                <p>Suplemen yang berasal dari ekstrak daging sapi.  Kandungan yang sangat membantu dalam proses pertumbuhan dan perkembangan otot..</p>
                                <a href="#" class="btn btn-primary">Beli</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="fh5co-blog animate-box">
                            <a href="#"><img class="img-responsive" src="images/3334.png" alt=""></a>
                            <div class="blog-text">
                                <h3><a href="" #>Suplemen Beta Alanin</a></h3>
                                <span class="posted_on">Rp. 600.000</span>
                                <span class="comment"><a href="">98<i class="icon-speech-bubble"></i></a></span>
                                <p>Manfaat dan fungsi dari suplemen ini yakni membantu menjaga keseimbangan pH otot selama latihan, menyangga akumulasi asam laktat, dan mengurangi kelelahan pada otot..</p>
                                <a href="#" class="btn btn-primary">Beli</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="fh5co-blog animate-box">
                            <a href="#"><img class="img-responsive" src="images/22222.png" alt=""></a>
                            <div class="blog-text">
                                <h3><a href="" #>Suplemen L-Carnitine</a></h3>
                                <span class="posted_on">Rp.750.000</span>
                                <span class="comment"><a href="">123<i class="icon-speech-bubble"></i></a></span>
                                <p>Carnitine adalah salah satu jenis asam amino non protein. Suplemen Carnitine berfungsi untuk membakar lemak sekaligus meningkatkan energi..</p>
                                <a href="#" class="btn btn-primary">Beli</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="fh5co-started" class="fh5co-bg" style="background-image: url(images/img_bg_3.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center">
						<h2>STAMINA GYM<br> <span> Gabung dan <br> Dapatkan </span> Diskon</span><span class="percent"> 35%</h2>
					</div>
				</div>
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center">
						<p><a href="loginuser.php" class="btn btn-default btn-lg">Jadi Member</a></p>
					</div>
				</div>
			</div>
		</div>

        <footer id="fh5co-footer" class="fh5co-bg" style="background-image: url(images/img_bg_1.jpg);" role="contentinfo">
            <div class="overlay"></div>
            <div class="container">
                <div class="row row-pb-md">
                    <div class="col-md-4 fh5co-widget">
                        <h3>Cerita Kecil tentang Stamina.</h3>
                        <p>Stamina adalah salah satu tempat kebugaran berbintang yang ada di bilangan Kota Jakarta. Stamina mulai didirikan sejak September 2020 dan kini keberadaanya banyak dinikmati oleh para Gymners</p>
                        <p><a class="btn btn-primary" href="loginuser.php">Menjadi Member</a></p>
                        <br>
                        <h3>Manage Website</h3>
                        <p><a class="btn btn-primary btn-sm" href="loginadmin.php">Login Admin</a></p>
                    </div>
                    <div class="col-md-8">
                        <h3>Kelas</h3>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <ul class="fh5co-footer-links">
                                <li><a href="#">Yoga</a></li>
                                <li><a href="#">Body Pump</a></li>
                            </ul>
                        </div>
                        <div class="col-md-1 col-sm-4 col-xs-6">
                            <h3>Peta</h3>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d253840.48788423213!2d106.68943048768607!3d-6.229728025806309!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3e945e34b9d%3A0x5371bf0fdad786a2!2sJakarta!5e0!3m2!1sen!2sid!4v1605943423854!5m2!1sen!2sid" width="330" height="330" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>

                <div class="row copyright">
                    <div class="col-md-12 text-center">
                        <p>
                            <small class="block"> copyright &copy; STAMINA GYM. All Rights reserved.</small>
                        </p>
                        <p>
                            <ul class="fh5co-social-icons">
                                <li><a href="#"><i class="icon-facebook"></i></a></li>
                                <li><a href="#"><i class="icon-instagram"></i></a></li>
                                <li><a href="#"><i class="icon-twitter"></i></a></li>
                                <li><a href="#"><i class="icon-youtube"></i></a></li>
                            </ul>
                        </p>
                    </div>
                </div>

            </div>
        </footer>
    </div>

    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
    </div>

    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>
    <!-- jQuery Easing -->
    <script src="js/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Waypoints -->
    <script src="js/jquery.waypoints.min.js"></script>
    <!-- Stellar Parallax -->
    <script src="js/jquery.stellar.min.js"></script>
    <!-- Carousel -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- countTo -->
    <script src="js/jquery.countTo.js"></script>
    <!-- Magnific Popup -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/magnific-popup-options.js"></script>
    <!-- Main -->
    <script src="js/main.js"></script>

</body>

</html>