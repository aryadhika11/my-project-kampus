<!DOCTYPE html>
<html lang="en">

<head>
	<title>STAMINA-Login Admin</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<!-- <link rel="icon" type="image/png" href="logintemp/images/icons/favicon.ico"/> -->
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="logintemp/vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="logintemp/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="logintemp/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="logintemp/vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="logintemp/vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="logintemp/vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="logintemp/css/util.css">
	<link rel="stylesheet" type="text/css" href="logintemp/css/main.css">
	<!--===============================================================================================-->
	<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
</head>

<body>


	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-t-190 p-b-30">
				<form class="login100-form validate-form" method="post" action="loginadmincheck.php">
					<div class="login100-form-avatar">
						<img src="logintemp/images/1.png" alt="AVATAR">
					</div>

					<span class="login100-form-title p-t-20 p-b-45">
						Login Admin
					</span>

					<div class="wrap-input100 validate-input m-b-10">
						<input class="input100" type="text" name="username" placeholder="Masukkan Username Anda">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input m-b-10" data-validate="Password harus diisi">
						<input class="input100" type="password" name="password" id="pass" placeholder="Masukkan Password Anda">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock"></i>
						</span>

					</div>

					<div class="wrap-input100 validate-input m-b-7">
						<p>Login Sebagai?</p>
						<select class="form-control" name="kategori" required>
							<option disabled="disabled" selected="selected">Pilih Kategori...</option>
							<option value="admin">Admin</option>
							<option value="owner">Owner</option>
						</select>
						<p style="color: white;">* Wajib Pilih Kategori</p>
					</div>

					<div class="container-login100-form-btn p-t-10">
						<button class="login100-form-btn"style="background-color: #F85915;" type="submit">
							Login
						</button>
					</div>

					<div class="text-center w-full p-t-25 p-b-230">
						<a href="ubahpasswordadmin.php" class="txt1">
							Lupa Password?
						</a>
					</div>
					<div class="text-center w-full">
						<small class="block"> copyright &copy; STAMINA GYM. All Rights reserved.</small>
					</div>
				</form>
			</div>
		</div>
	</div>
	</div>
</body>



<!--===============================================================================================-->
<script src="logintemp/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="logintemp/vendor/bootstrap/js/popper.js"></script>
<script src="logintemp/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="logintemp/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="logintemp/js/main.js"></script>


</html>