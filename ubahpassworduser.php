<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--===============================================================================================-->
    <!-- <link rel="icon" type="image/png" href="logintemp/images/icons/favicon.ico"/> -->
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logintemp/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logintemp/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logintemp/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logintemp/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logintemp/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logintemp/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="logintemp/css/util.css">
    <link rel="stylesheet" type="text/css" href="logintemp/css/main.css">
    <!--===============================================================================================-->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>STAMINA--UbahPassword</title>
</head>

<body>
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100 p-t-190 p-b-30">
                <div class="login100-form-avatar">
                    <img src="logintemp/images/2.png" alt="AVATAR">
                </div>
                <span class="login100-form-title p-t-20 p-b-45">
                    Ubah Password
                </span>
                <div class="wrap-input100 validate-input m-b-10">
                    <!-- <div class="card-body"> -->
                    <form method="POST" action="ubahpasswordcheckuser.php">

                        <div class="wrap-input100 validate-input m-b-10">
                            <label for="exampleInputEmail1">Password Lama</label>
                            <input type="password" class="form-control" id="exampleInputEmail1" maxlength="10" aria-describedby="emailHelp" name="pass_lama" placeholder="Masukkan Password Lama Anda" required>
                            <small id="emailHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="wrap-input100 validate-input m-b-10">
                            <label for="exampleInputEmail2">Password Baru</label>
                            <input type="password" class="form-control" id="exampleInputEmail2" maxlength="10" aria-describedby="emailHelp" name="pass_baru" placeholder="Masukkan Password Baru Anda" required>
                            <small id="emailHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="wrap-input100 validate-input m-b-10">
                            <label for="exampleInputEmail3">Konfirmasi Password Baru</label>
                            <input type="password" class="form-control" id="exampleInputEmail3" maxlength="10" aria-describedby="emailHelp" name="konfirmasi_pass" placeholder="Masukkan Konfirmasi Password Baru Anda" required>
                            <small id="emailHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="container-login100-form-btn p-t-10">
                            <button type="submit" style="background-color: red;" class="login100-form-btn">Proses</button>
                        </div>
                        <center>Or</center>
                        <div class="container-login100-form-btn p-t-10">
                            <button type="reset" class="login100-form-btn">Reset</button>
                        </div>
                        <br><br><br><br>
                        <div class="text-center w-full">
                            <small class="block"> copyright &copy; STAMINA GYM. All Rights reserved.</small>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>

<!--===============================================================================================-->
<script src="logintemp/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="logintemp/vendor/bootstrap/js/popper.js"></script>
<script src="logintemp/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="logintemp/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="logintemp/js/main.js"></script>

</html>