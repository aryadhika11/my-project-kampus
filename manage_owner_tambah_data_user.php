<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php

include 'koneksi.php';
$sql = mysqli_query($koneksi, "SELECT * FROM admindnowner WHERE kategori = 'owner' ");
$dika = mysqli_fetch_assoc($sql);
?>


<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>STAMINA--Manage Admin</title>
    <!-- Bootstrap Styles-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="fh5co-logo">
                    <a class="navbar-brand" href="manage_admin.php"><strong>STAMINA <span>.</span> </strong></a></div>
            </div>

            <ul class="nav navbar-top-links navbar-right">

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="manage_owner_userprofile.php"><i class="fa fa-user fa-fw"></i> Nyonya <?php echo $dika['username'] ?> profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout.php" onclick="return confirm('Ingin Logout?')"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>

                </li>

            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div id="sideNav" href=""><i class="fa fa-caret-right"></i></div>
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li>
                        <a href="manage_owner.php"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>

                    <li>
                        <a class="active-menu" href="laporanowner.php"><i class="fa fa-file-o"></i> Laporan </a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Hallo Nyonya <?php echo $dika['username'] ?>!
                        </h1>

                    </div>
                </div>


                <!-- /. ROW  -->

                <div class="row">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <h2>Laporan</h2>
                            <br>
                            <h2 style="text-align: center;">Data Pengunjung</h2>
                            <br>
                            <form action="" method="POST">

                                <button type="submit" value="update" name="update" class="btn btn-primary">Proses</button>
                                <br>
                                <br>
                                <div class="form-row">

                                    <div id="data">

                                        <div class="form-group col-md-6">
                                            <label for="namadep">Nama Depan</label>
                                            <input type="text" class="form-control" name="namadep" id="namadep" placeholder="ex : Arya" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="namabel">Nama Belakang</label>
                                            <input type="text" class="form-control" id="namabel" name="namabel" placeholder="ex : dika" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" name="email" id="email" required placeholder="ex : example@domain.com">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="notlp">No Telepon</label>
                                            <input type="tel" class="form-control" name="notlp" id="notlp" maxlength="11" required placeholder="ex : 08xxxxxxxxx">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="username">Username</label>
                                            <input type="username" class="form-control" name="username" required id="username">
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" maxlength="8" value="password" required name="password" id="password">
                                            <label>
                                                <input type="checkbox" name="" onclick="myfunction()"> Lihat Password
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </form>
                            <?php
                            include "koneksi.php";
                            if(isset($_POST['update'])){
                            $username = $_POST['username'];
                            $cek = mysqli_query($koneksi, "SELECT * FROM datainput WHERE id");
                            $ambil = mysqli_fetch_assoc($cek);
                            if($username != $ambil['username']){
                                $input = mysqli_query($koneksi, "INSERT INTO datainput VALUES(id, '".$_POST['namadep']."', '".$_POST['namabel']."', '".$_POST['email']."', '".$_POST['notlp']."', '".$_POST['username']."', '".$_POST['password']."')");
                                if($input){
                                    echo '<script>alert("Data Berhasil ditambahkan");document.location="laporan_owner_data_user.php";</script>';
                                }else{
                                    echo '<script>alert("Data Gagal ditambahkan");document.location="manage_owner_tambah_data_user.php";</script>';
                                }
                            }else{
                                echo '<script>alert("Maaf, Username telah digunakan");document.location="manage_owner_tambah_data_user.php";</script>';
                            }
                        }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function myfunction() {
                var pass = document.getElementById('password')
                if (pass.type == 'password') {
                    pass.type = 'text';
                } else {
                    pass.type = 'password';
                }
            }
        </script>
        <!-- /. ROW  -->
        <footer>
            <div class="text-center w-full">
                <small class="block"> copyright &copy; STAMINA GYM. All Rights reserved.</small>
            </div>
        </footer>
    </div>
    <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- Bootstrap Js -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>


    <script src="assets/js/easypiechart.js"></script>
    <script src="assets/js/easypiechart-data.js"></script>


    <!-- Custom Js -->
    <script src="assets/js/custom-scripts.js"></script>


</body>

</html>