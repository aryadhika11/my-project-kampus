<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php

include 'koneksi.php';
$sql = mysqli_query($koneksi, "SELECT * FROM admindnowner WHERE kategori = 'owner' ");
$dika = mysqli_fetch_assoc($sql);
?>




<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>STAMINA--Manage Admin</title>
    <!-- Bootstrap Styles-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="fh5co-logo">
                    <a class="navbar-brand" href="manage_admin.php"><strong>STAMINA <span>.</span> </strong></a></div>
            </div>

            <ul class="nav navbar-top-links navbar-right">

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="manage_owner_userprofile.php"><i class="fa fa-user fa-fw"></i> Nyonya <?php echo $dika['username'] ?> profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout.php" onclick="return confirm('Ingin Logout?')"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>

                </li>

            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div id="sideNav" href=""><i class="fa fa-caret-right"></i></div>
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li>
                        <a href="manage_owner.php"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>

                    <li>
                        <a class="active-menu" href="laporanowner.php"><i class="fa fa-file-o"></i> Laporan </a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Hallo Nyonya <?php echo $dika['username'] ?>!
                        </h1>

                    </div>
                </div>


                <!-- /. ROW  -->

                <div class="row">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <h2>Laporan</h2>
                            <br>
                            <ol class="breadcrumb">
                                <li class="active"><a href="laporanowner.php">Data Admin</a></li>
                                <li><a href="laporan_owner_data_user.php">Data Pengunjung</a></li>
                                <li><a href="laporan_owner_data_penjualan.php">Data Penjualan</a></li>
                                <li><a href="laporan_owner_tanggapan.php">Data Tanggapan</a></li>
                            </ol>
                            <h2 style="text-align: center;">Data Admin & Owner</h2>
                            <br>
                            <a href="manage_owner_tambah_data.php"><button type="button" class="btn btn-primary btn-md active" style="color: white;"> Tambah Data</button></a>
                            ||
                            <a href="cetakadmin.php"><button type="button" class="btn btn-success btn-md active" style="color: white;">Cetak</button></a>
                            ||
                            || <a href="hapussemuadataadmin.php" onclick="return confirm('Ingin hapus semua data?')"><button type="button" class="btn btn-danger btn-md active" style="color: white;"> Hapus Semua</button></a>
                            <form class="form-inline" action="caridiowner.php" method="GET" style="float: right;">
                                <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search">
                                <button class="btn btn-primary my-2 my-sm-0" name="submit" style="margin-right: 30px;" type="submit">Search</button>
                            </form>
                            <br>
                            <br>
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Nama Lengkap</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Lama Kerja</th>
                                        <th>Posisi</th>
                                        <th>Pengalaman_Kerja</th>
                                        <th>Username</th>
                                        <th>Kategori</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    include "koneksi.php";
                                    $sql = mysqli_query($koneksi, "SELECT * FROM admindnowner   ORDER BY id ASC");
                                    if (mysqli_num_rows($sql) > 0) {

                                        while ($data = mysqli_fetch_assoc($sql)) {
                                            echo '
                                                <tr>
                                                    <td>' . $data['id'] . '</td>
                                                    <td>' . $data['nama_lengkap'] . '</td>
                                                    <td>' . $data['ttl'] . '</td>
                                                    <td>' . $data['jk'] . '</td>
                                                    <td>' . $data['lama_kerja'] . '</td>
                                                    <td>' . $data['posisi'] . '</td>
                                                    <td>' . $data['pengalaman_kerja'] . '</td>
                                                    <td>' . $data['username'] . '</td>
                                                    <td>' . $data['kategori'] . '</td>
                                                    <td>
                                                    <a href = "manage_owner_edit.php?id='.$data['id'].'" name="edit"  style ="background-color : #D5E3EB" class="btn  btn-sm">Edit </a>
                                                    |
                                                    <a href = "manage_owner_delete_admin.php?id='.$data['id'].'" class = "btn btn-danger btn-sm" onclick ="return confirm(\'Yakin Ingin menghapus data ini?\')">Hapus </a>
                                                    <td>
                                                    
                                                </tr>
                                                    ';
                                        }
                                    } else {
                                        echo '<tr>
                                            <td colspan="6" style="text-align: center;">Tidak ada data. </td>
                                            </tr>
                                            ';
                                    }
                                    ?>
                                </tbody>
                                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">STAMINA GYM</h5>

                                            </div>
                                            <div class="modal-body">
                                                <?php
                                                $sqli = mysqli_query($koneksi, "SELECT * FROM admindnowner WHERE kategori = 'owner' ");
                                                $ambil = mysqli_fetch_assoc($sqli);
                                                ?>
                                                Data Berhasil dikirim ke Nyonya <?php echo $ambil['username'] ?>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </table>

                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!-- /. ROW  -->
        <footer>
            <div class="text-center w-full">
                <small class="block"> copyright &copy; STAMINA GYM. All Rights reserved.</small>
            </div>
        </footer>
    </div>
    <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- Bootstrap Js -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>


    <script src="assets/js/easypiechart.js"></script>
    <script src="assets/js/easypiechart-data.js"></script>


    <!-- Custom Js -->
    <script src="assets/js/custom-scripts.js"></script>


</body>

</html>