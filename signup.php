<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>STAMINA-Form SignUp</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">


    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
</head>

<body>

    <form action="register.php" method="post">
        <p><span class="error"></span></p>
        <div class="page-wrapper p-t-130 p-b-100 font-poppins" style="background-color: #56CCF2;">
            <div class="wrapper wrapper--w680">
                <div class="card card-4">
                    <div class="card-body">
                        <h2 class="title">SignUp Akun</h2>
                        <p><span class="error"></span></p>
                        <form method="POST" action="register.php">
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label" for="namadep">Nama Depan</label>
                                        <input class="input--style-4" type="text" id="namadep" name="namadep" required>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label" for="namabel">Nama Belakang</label>
                                        <input class="input--style-4" type="text" id="namabel" name="namabel" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-space">
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label" for="email">Email</label>
                                        <input class="input--style-4" type="email" id="email" name="email" required>
                                        <p style="font-size: 12px;">Contoh : example@domain.com</p>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label" for="notlp">Nomor Telepon</label>
                                        <input class="input--style-4" type="tel" id="notlp" maxlength="12" name="notlp">
                                        <p style="font-size: 12px;">Contoh : 08xxxxxxxxxx</p>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label" for="username">Username</label>
                                        <input class="input--style-4" type="username" id="username" maxlength="20" name="username" required>
                                        <p style="font-size: 12px;"></p>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="input-group">
                                        <label class="label" for="password">password</label>
                                        <input class="input--style-4" type="password" id="password" maxlength="10" name="password" required>
                                        <p style="font-size: 12px;"></p>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="p-t-15">
                                <input type="submit" name="submit" value="Submit" class="btn btn--radius-2 btn--blue">
                                <center>Or</center>
                                <input class="btn btn--radius-2 btn--green" type="Reset" value="Reset">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="text-center">
                    <center>
                        <small class="block"> copyright &copy; STAMINA GYM. All Rights reserved.</small>
                    </center>
                </div>
            </div>
        </div>
    </form>


    ?>
    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>