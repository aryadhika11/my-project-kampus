<!DOCTYPE HTML>
<html>
<?php
include "koneksi.php";
session_start();
error_reporting(0);
ini_set('display_errors', 0);
if ($id = $_SESSION['id']){
	$sqli = mysqli_query($koneksi, "SELECT * FROM datainput WHERE id = '$id' ");
	$panggil = mysqli_fetch_assoc($sqli);	
}else{
	echo '<script>alert("Maaf Anda belum melakukan Login");document.location="loginuser.php";</script>';
}
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>STAMINA</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free HTML5 Website Template by freehtml5.co" />
    <meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
    <meta name="author" content="freehtml5.co" />

    <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FreeHTML5.co
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content="" />
    <meta property="og:image" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:description" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="css/bootstrap.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">

    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">


    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>

    <div class="fh5co-loader"></div>

    <div id="page">
        <nav class="fh5co-nav" role="navigation">
            <div class="top">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <p class="num">Call: <a href="https://web.whatsapp.com/">+62 5710607708</a></p>
                            <ul class="fh5co-social">
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-instagram"></i></a></li>
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-youtube"></i></a></li>
								||
								<li><a href="kantongbelanja.php" style="color: white;"><i class="icon-shop"> Keranjang</i></a></li>
								||
								<li><a href="userprofile.php?id=<?php echo $panggil['id']?>" style="color: white;"><i class="icon-user"> Profile</i></a></li>
								||
								<li><a href="logout2.php" onclick="return confirm('Ingin Logout?')"><i class="icon-power"> Logout</i></a></li>
							</ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top-menu">
                <div class="container">
                    <div class="row">
                    <div class="col-xs-2">
							<div id="fh5co-logo"><a href="indexprofile.php">Stamina<span>.</span></a></div>
						</div>
						<div class="col-xs-10 text-right menu-1">
							<ul>
								<li><a href="indexprofile.php">Home</a></li>
								<li><a href="keanggotaanprofile.php">Keanggotaan</a></li>
								<li><a href="aboutusprofile.php">About Us</a></li>
								<li class="has-dropdown">
									<a>Informasi</a>
									<ul class="dropdown">
										<li><a href="jenissuplemenprofile.php">Jenis Suplement</a></li>
										<li><a href="informasigiziprofile.php">Informasi gizi</a></li>
										<li><a href="dietprofile.php">Diet</a></li>
										<li><a href="kontakprofile.php">Kontak Kami</a></li>
									</ul>
								</li>
								<li class="has-dropdown" class="active">
									<a>Program</a>
									<ul class="dropdown">
										<li><a href="programprofile.php">Kelas Yoga</a></li>
										<li><a href="bodypumpprofile.php">Kelas Body Pump</a></li>
										<li><a href="videogymprofile.php">Video Gym</a></li>
									</ul>
								</li>
								<li><a href="promosiprofile.php">Promosi</a></li>
							</ul>
						</div>
                    </div>

                </div>
            </div>
        </nav>

        <header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(images/les-mills-body-pump-mobile.jpg);" data-stellar-background-ratio="0.5">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <div class="display-t">
                            <div class="display-tc animate-box" data-animate-effect="fadeIn">
                                <h1>Kelas</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>


        <div id="fh5co-pricing">
            <div class="container">
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                        <h2>Body Pump</h2>
                        <p><b>Body Pump </b> adalah olahraga berkelompok berdurasi 30-60 menit per sesi dengan tujuan membentuk otot bagi pemula dan pengencangan otot bagi mereka yang sudah terbiasa olahraga. Dalam satu sesi, terdapat 10 lagu dengan fokus pada salah satu bagian tubuh per lagu.
                        </p>
                    </div>
                    <div class="row animate-box">
                        <div class="row row-cols-1 row-cols-md-3 g-4">
                            <div class="col">
                                <div class="card h-100">
                                    <div class="img-responsive" style="width: 400px;" style="height: 200px;">
                                        <img src="images/bg1.png" class="card-img-top" alt="..."></div>
                                    <div class="card-body">
                                        <h4><bstyle= "color: #F85915;">Angkat beban untuk wanita </b></h4>
                                        <p class="card-text">Perlu kita ketahui bahwa peningkatan otot pada wanita hanya akan menonjolkan penampilan feminin mereka. Wanita tidak akan memiliki otot seperti pria. Wanita tidak akan menjadi “jantan” jika mereka berlatih beban, karena wanita tidak memiliki hormon yang dapat membentuk otot-otot seperti yang ada pada pria, kecuali jika wanita tersebut diberi penguat hormon seperti steroid anabolik.</p>
                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted">Terakhir di update 2 menit yang lalu</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card h-100">
                                    <img src="images/bg2.png" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">Card title</h5>
                                        <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted">Terakhir di update 3 menit yang lalu</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card h-100">
                                    <img src="images/bp3.jpg" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">Card title</h5>
                                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted">Terakhir di update 5 menit yang lalu</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row animate-box">
                        <div class="row row-cols-1 row-cols-md-3 g-4">
                            <div class="col">
                                <div class="card h-100">
                                    <img src="images/bp4.jpg" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">Card title</h5>
                                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted">Terakhir di update 6 menit yang lalu</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card h-100">
                                    <img src="images/bp5.jpg" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">Card title</h5>
                                        <p class="card-text">This card has supporting text below as a natural lead-in to additional content.</p>
                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted">Terakhir di update 7 menit yang lalu</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card h-100">
                                    <img src="images/bp6.jpg" class="card-img-top" alt="...">
                                    <div class="card-body">
                                        <h5 class="card-title">Card title</h5>
                                        <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p>
                                    </div>
                                    <div class="card-footer">
                                        <small class="text-muted">Terakhir di update 8 menit yang lalu</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row animate-box">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <br>
                        <p><a href="signup.php" class="btn btn-default btn-lg" style="background-color: #F85915;"><i style="color: white;">Gabung Sekarang</i></a><p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="fh5co-started" class="fh5co-bg" style="background-image: url(images/img_bg_3.jpg);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2>KELAS FITNESS TELAH DIBUKA<br> <span>COBA SEKARANG DAN <br> DAPATKAN DISKON</span><span class="percent"> 35%</span></h2>
                </div>
            </div>
            <div class="row animate-box">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <p><a href="programprofile.php" class="btn btn-default btn-lg"><i>Lihat Kelas</a></p>
                </div>
            </div>
        </div>
    </div>


    <footer id="fh5co-footer" class="fh5co-bg" style="background-image: url(images/img_bg_1.jpg);" role="contentinfo">
        <div class="overlay"></div>
        <div class="container">
            <div class="row row-pb-md">
            <div class="col-md-4 fh5co-widget">
					<h3>Cerita Kecil tentang Stamina.</h3>
					<p>Stamina adalah salah satu tempat kebugaran berbintang yang ada di bilangan Kota Jakarta. Stamina mulai didirikan sejak September 2020 dan kini keberadaanya banyak dinikmati oleh para Gymners</p>
					<p><a class="btn btn-primary" href="promosiprofile.php">Promosi</a></p>
					<br>
				</div>
                <div class="col-md-8">
                    <h3>Kelas</h3>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <ul class="fh5co-footer-links">
                            <li><a href="program.php">Yoga</a></li>
                            <li><a href="bodypump.php">Body Pump</a></li>
                        </ul>
                    </div>
                    <div class="col-md-1 col-sm-4 col-xs-6">
                        <h3>Peta</h3>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d253840.48788423213!2d106.68943048768607!3d-6.229728025806309!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3e945e34b9d%3A0x5371bf0fdad786a2!2sJakarta!5e0!3m2!1sen!2sid!4v1605943423854!5m2!1sen!2sid" width="330" height="330" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div>

            <div class="row copyright">
                <div class="col-md-12 text-center">
                    <p>
                        <small class="block"> copyright &copy; STAMINA GYM. All Rights reserved.</small>
                    </p>
                    <p>
                        <ul class="fh5co-social-icons">
                            <li><a href="#"><i class="icon-facebook"></i></a></li>
                            <li><a href="#"><i class="icon-instagram"></i></a></li>
                            <li><a href="#"><i class="icon-twitter"></i></a></li>
                            <li><a href="#"><i class="icon-youtube"></i></a></li>
                        </ul>
                    </p>
                </div>
            </div>

        </div>
    </footer>
    </div>

    <div class="gototop js-top">
        <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
    </div>

    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>
    <!-- jQuery Easing -->
    <script src="js/jquery.easing.1.3.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Waypoints -->
    <script src="js/jquery.waypoints.min.js"></script>
    <!-- Stellar Parallax -->
    <script src="js/jquery.stellar.min.js"></script>
    <!-- Carousel -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- countTo -->
    <script src="js/jquery.countTo.js"></script>
    <!-- Magnific Popup -->
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/magnific-popup-options.js"></script>
    <!-- Main -->
    <script src="js/main.js"></script>

</body>

</html>