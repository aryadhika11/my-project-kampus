<!DOCTYPE HTML>
<html>
<?php
include "koneksi.php";
session_start();
error_reporting(0);
ini_set('display_errors', 0);
if ($id = $_SESSION['id']){
	$sqli = mysqli_query($koneksi, "SELECT * FROM datainput WHERE id = '$id' ");
	$panggil = mysqli_fetch_assoc($sqli);	
}else{
	echo '<script>alert("Maaf Anda belum melakukan Login");document.location="loginuser.php";</script>';
}
?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>STAMINA GYM</title>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by freehtml5.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="freehtml5.co" />

	<!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FreeHTML5.co
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="" />
	<meta property="og:image" content="" />
	<meta property="og:url" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800" rel="stylesheet">

	<!-- CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style2.css">

	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">


	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- videopopup -->
	<link rel="stylesheet" href="../css/style2.css">
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>

	<div class="fh5co-loader"></div>

	<div id="page">
		<nav class="fh5co-nav" role="navigation">
			<div class="top">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 text-right">
							<p class="num">Call: <a href="https://web.whatsapp.com/">+62 5710607708</a></p>
							<ul class="fh5co-social">
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-instagram"></i></a></li>
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-youtube"></i></a></li>
								||
								<li><a href="kantongbelanja.php" style="color: white;"><i class="icon-shop"> Keranjang</i></a></li>
								||
								<li><a href="userprofile.php?id=<?php echo $panggil['id']?>" style="color: white;"><i class="icon-user"> Profile</i></a></li>
								||
								<li><a href="logout2.php" onclick="return confirm('Ingin Logout?')"><i class="icon-power"> Logout</i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="top-menu">
				<div class="container">
					<div class="row">
					<div class="col-xs-2">
							<div id="fh5co-logo"><a href="indexprofile.php">Stamina<span>.</span></a></div>
						</div>
						<div class="col-xs-10 text-right menu-1">
							<ul>
								<li><a href="indexprofile.php">Home</a></li>
								<li><a href="keanggotaanprofile.php">Keanggotaan</a></li>
								<li><a href="aboutusprofile.php">About Us</a></li>
								<li class="has-dropdown">
									<a>Informasi</a>
									<ul class="dropdown">
										<li><a href="jenissuplemenprofile.php">Jenis Suplement</a></li>
										<li><a href="informasigiziprofile.php">Informasi gizi</a></li>
										<li><a href="dietprofile.php">Diet</a></li>
										<li><a href="kontakprofile.php">Kontak Kami</a></li>
									</ul>
								</li>
								<li class="has-dropdown" class="active">
									<a>Program</a>
									<ul class="dropdown">
										<li><a href="programprofile.php">Kelas Yoga</a></li>
										<li><a href="bodypumpprofile.php">Kelas Body Pump</a></li>
										<li><a href="videogymprofile.php">Video Gym</a></li>
									</ul>
								</li>
								<li><a href="promosiprofile.php">Promosi</a></li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</nav>


		<div id="fh5co-started" class="fh5co-bg" style="background-image: url(images/about-us-bg.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center">
						<h2>STAMINA GYM<br> <span> Gabung dan <br> Dapatkan </span> Diskon</span><span class="percent"> 35%</h2>
					</div>
				</div>
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center">
						<p><a href="bodypumpprofile.php" class="btn btn-default btn-lg">Lihat Kelas</a></p>
					</div>
				</div>
			</div>
		</div>
		<div id="fh5co-pricing">
			<div class="container">
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
						<h2><b style="color: #F85915;">Lihat!</b> Latihan Gym Selama<b style="color: #F85915;"><i> WFH</b></i></h2>
						<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								<li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
								<li data-target="#carouselExampleCaptions2" data-slide-to="1"></li>
								<li data-target="#carouselExampleCaptions3" data-slide-to="2"></li>
							</ol>
							<div class="carousel-inner">
								<div class="carousel-item active">
									<img src="images/ss.png" class="d-block w-100" alt="..." data-toggle="modal" data-target="#exampleModalCenter">
									<div class="carousel-caption d-none d-md-block">
										<p style="color: #F85915;">Cara Bikin Alat Gym Selama WFH.</p>
									</div>
								</div>
								<div class="carousel-item">
									<img src="images/ss2.png" class="d-block w-100" alt="..." data-toggle="modal" data-target="#exampleModalCenter2">
									<div class="carousel-caption d-none d-md-block">
										<p style="color: #F85915;">Bentuk Otot Dada dengan alat seadanya selama WFH.</p>
									</div>
								</div>
								<div class="carousel-item">
									<img src="images/ss3.png" class="d-block w-100" alt="..." data-toggle="modal" data-target="#exampleModalCenter3">
									<div class="carousel-caption d-none d-md-block">
										<p style="color: #F85915;">5 Exercise yang bisa dilakukan selama WFH.</p>
									</div>
								</div>
							</div>
							<a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php

	
	?>
	<div class="modal fade" id="exampleModalCenter">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<video width="100%" controls>
						<source  src="video/video1.mp4" type="video/mp4">
					</video>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="exampleModalCenter2">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<video width="100%" controls>
						<source src="video/video2.mp4" type="video/mp4">
					</video>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="exampleModalCenter3">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<video width="100%" controls>
						<source src="video/video3.mp4" type="video/mp4">
					</video>
				</div>
			</div>
		</div>
	</div>

	<footer id="fh5co-footer" class="fh5co-bg" style="background-image: url(images/img_bg_1.jpg);" role="contentinfo">
		<div class="overlay"></div>
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-4 fh5co-widget">
					<h3>Cerita Kecil tentang Stamina.</h3>
					<p>Stamina adalah salah satu tempat kebugaran berbintang yang ada di bilangan Kota Jakarta. Stamina mulai didirikan sejak September 2020 dan kini keberadaanya banyak dinikmati oleh para Gymners</p>
					<p><a class="btn btn-primary" href="promosiprofile.php">Promosi</a></p>
					<br>
				</div>
				<div class="col-md-8">
					<h3>Kelas</h3>
					<div class="col-md-4 col-sm-4 col-xs-6">
						<ul class="fh5co-footer-links">
							<li><a href="#">Yoga</a></li>
							<li><a href="#">Body Pump</a></li>
						</ul>
					</div>
					<div class="col-md-1 col-sm-4 col-xs-6">
						<h3>Peta</h3>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d253840.48788423213!2d106.68943048768607!3d-6.229728025806309!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3e945e34b9d%3A0x5371bf0fdad786a2!2sJakarta!5e0!3m2!1sen!2sid!4v1605943423854!5m2!1sen!2sid" width="330" height="330" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
					</div>
				</div>
			</div>

			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block"> copyright &copy; STAMINA GYM. All Rights reserved.</small>
					</p>
					<p>
						<ul class="fh5co-social-icons">
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-instagram"></i></a></li>
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-youtube"></i></a></li>
						</ul>
					</p>
				</div>
			</div>

		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>

</body>

</html>