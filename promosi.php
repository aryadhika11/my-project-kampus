<!DOCTYPE HTML>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>STAMINA</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Website Template by freehtml5.co" />
	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
	<meta name="author" content="freehtml5.co" />

	<!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FreeHTML5.co
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="" />
	<meta property="og:image" content="" />
	<meta property="og:url" content="" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800" rel="stylesheet">

	<!-- CSS -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style2.css">

	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">


	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- videopopup -->
	<link rel="stylesheet" href="../css/style2.css">
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>

<body>

	<div class="fh5co-loader"></div>

	<div id="page">
		<nav class="fh5co-nav" role="navigation">
			<div class="top">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 text-right">
							<p class="num">Call: <a href="https://web.whatsapp.com/">+62 5710607708</a></p>
							<ul class="fh5co-social">
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-instagram"></i></a></li>
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-youtube"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="top-menu">
				<div class="container">
					<div class="row">
						<div class="col-xs-2">
							<div id="fh5co-logo"><a href="index.php">Stamina<span>.</span></a></div>
						</div>
						<div class="col-xs-10 text-right menu-1">
							<ul>
								<li><a href="index.php">Home</a></li>
								<li><a href="keanggotaan.php">Keanggotaan</a></li>
								<li><a href="aboutus.php">About Us</a></li>
								<li class="has-dropdown">
									<a>Informasi</a>
									<ul class="dropdown">
										<li><a href="jenissuplemen.php">Jenis Suplement</a></li>
										<li><a href="informasigizi.php">Informasi gizi</a></li>
										<li><a href="diet.php">Diet</a></li>
										<li><a href="kontak.php">Kontak Kami</a></li>
									</ul>
								</li>
								<li class="has-dropdown">
									<a>Program</a>
									<ul class="dropdown">
										<li><a href="program.php">Kelas Yoga</a></li>
										<li><a href="bodypump.php">Kelas Body Pump</a></li>
										<li><a href="videogym.php">Video Gym</a></li>
									</ul>
								</li>
								<li class="active"><a href="promosi.php">Promosi</a></li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</nav>

		<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(images/about-us-bg.jpg);" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center">
						<div class="display-t">
							<div class="display-tc animate-box" data-animate-effect="fadeIn">
								<h1>Promosi</h1>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>


		<div id="fh5co-blog" class="fh5co-bg-section">
			<div class="container">
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
						<h2>PROMOSI</h2>
						<div class="row">
							<div class="card mb-3" style="max-width: 1000px;">
								<div class="row no-gutters">
									<div class="col-md-4">
										<img src="images/promo.png" class="card-img" alt="...">
									</div>
									<div class="col-md-8">
										<div class="card-body">
											<h3 class="card-title">DISCOUNT <b style="color: #F85915;"> 50% </b> UNTUK SEMUA KELAS!!</h3>
											<h4><i>Jangan sampai kelewatan!</i></h4>
											<p class="card-text"><b style="color: black;"> S&K :</b> <br>- Promo hanya pada tanggal 5 Desember 2020 </br></p>
											<button type="button" class="btn btn-outline-dark" style="color: #F85915;">Pesan Sekarang</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row animate-box">
							<div class="card mb-3" style="max-width: 1000px;">
								<div class="row no-gutters">
									<div class="col-md-4">
										<img src="images/promo2.png" class="card-img" alt="...">
									</div>
									<div class="col-md-8">
										<div class="card-body">
											<h3 class="card-title"><b style="color: #F85915;">LIMITED</b> <b>ORDER!</b></h3>
											<h4><i>Pesanan terbatas untuk sumplemen</i></h4>
											<p class="card-text"><b style="color: black;"> S&K :</b> <br>- Hanya berlaku metode pembayaran M-Banking</br></p>
											<button type="button" class="btn btn-outline-dark" style="color: #F85915;">Pesan Sekarang</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row animate-box">
							<div class="card mb-3" style="max-width: 1000px;">
								<div class="row no-gutters">
									<div class="col-md-4">
										<img src="images/promosi3.png" class="card-img" alt="...">
									</div>
									<div class="col-md-8">
										<div class="card-body">
											<h3 class="card-title">SHOP NOW <b style="color: #F85915;">25% OFF</b></h3>
											<h4><i>Lebih hemat 25% setiap pembelian suplemen</i></h4>
											<p class="card-text"><b style="color: black;"> S&K :</b> <br>- - Promo hanya pada bulan Desember 2020</br></p>
											<button type="button" class="btn btn-outline-dark" style="color: #F85915;">Pesan Sekarang</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<br>
						<a href="#" class="animated-box"><h4> Show More <div class="fa fa-sort-desc"></div></h4></a>
						
					</div>
				</div>
			</div>


			<div id="fh5co-started" class="fh5co-bg" style="background-image: url(images/img_bg_3.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center">
						<h2>STAMINA GYM<br> <span> Gabung dan <br> Dapatkan </span> Diskon</span><span class="percent"> 35%</h2>
					</div>
				</div>
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center">
						<p><a href="loginuser.php" class="btn btn-default btn-lg">Jadi Member</a></p>
					</div>
				</div>
			</div>
		</div>


			<footer id="fh5co-footer" class="fh5co-bg" style="background-image: url(images/img_bg_1.jpg);" role="contentinfo">
				<div class="overlay"></div>
				<div class="container">
					<div class="row row-pb-md">
						<div class="col-md-4 fh5co-widget">
							<h3>Cerita Kecil tentang Stamina.</h3>
							<p>Stamina adalah salah satu tempat kebugaran berbintang yang ada di bilangan Kota Jakarta. Stamina mulai didirikan sejak September 2020 dan kini keberadaanya banyak dinikmati oleh para Gymners</p>
							<p><a class="btn btn-primary" href="loginuser.php">Menjadi Member</a></p>
							<br>
							<h3>Manage Website</h3>
							<p><a class="btn btn-primary btn-sm" href="loginadmin.php">Login Admin</a></p>
						</div>
						<div class="col-md-8">
							<h3>Kelas</h3>
							<div class="col-md-4 col-sm-4 col-xs-6">
								<ul class="fh5co-footer-links">
									<li><a href="program.php">Yoga</a></li>
									<li><a href="bodypump.php">Body Pump</a></li>
								</ul>
							</div>
							<div class="col-md-1 col-sm-4 col-xs-6">
								<h3>Peta</h3>
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d253840.48788423213!2d106.68943048768607!3d-6.229728025806309!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3e945e34b9d%3A0x5371bf0fdad786a2!2sJakarta!5e0!3m2!1sen!2sid!4v1605943423854!5m2!1sen!2sid" width="330" height="330" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
							</div>
						</div>
					</div>

					<div class="row copyright">
						<div class="col-md-12 text-center">
							<p>
								<small class="block"> copyright &copy; STAMINA GYM. All Rights reserved.</small>
							</p>
							<p>
								<ul class="fh5co-social-icons">
									<li><a href="#"><i class="icon-facebook"></i></a></li>
									<li><a href="#"><i class="icon-instagram"></i></a></li>
									<li><a href="#"><i class="icon-twitter"></i></a></li>
									<li><a href="#"><i class="icon-youtube"></i></a></li>
								</ul>
							</p>
						</div>
					</div>

				</div>
			</footer>
		</div>

		<div class="gototop js-top">
			<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
		</div>

		<!-- jQuery -->
		<script src="js/jquery.min.js"></script>
		<!-- jQuery Easing -->
		<script src="js/jquery.easing.1.3.js"></script>
		<!-- Bootstrap -->
		<script src="js/bootstrap.min.js"></script>
		<!-- Waypoints -->
		<script src="js/jquery.waypoints.min.js"></script>
		<!-- Stellar Parallax -->
		<script src="js/jquery.stellar.min.js"></script>
		<!-- Carousel -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- countTo -->
		<script src="js/jquery.countTo.js"></script>
		<!-- Magnific Popup -->
		<script src="js/jquery.magnific-popup.min.js"></script>
		<script src="js/magnific-popup-options.js"></script>
		<!-- Main -->
		<script src="js/main.js"></script>

</body>

</html>