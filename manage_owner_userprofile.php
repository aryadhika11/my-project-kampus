<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php

include "koneksi.php";

session_start();

$cek_user = mysqli_query($koneksi, "SELECT * FROM admindnowner WHERE   kategori = 'owner' ");
$pecah = mysqli_fetch_assoc($cek_user);
?>



?>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>STAMINA--Manage Owner</title>
    <!-- Bootstrap Styles-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="fh5co-logo">
                    <a class="navbar-brand" href="manage_admin.php"><strong>STAMINA <span>.</span> </strong></a></div>
            </div>

            <ul class="nav navbar-top-links navbar-right">

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> <?php echo $pecah['username'] ?> Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>

                </li>

            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div id="sideNav" href=""><i class="fa fa-caret-right"></i></div>
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li>
                        <a  href="manage_owner.php"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="laporanowner.php"><i class="fa fa-file-o"></i> Laporan </a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">

                            Profile <?php echo $pecah['username'] ?>
                        </h1>
                    </div>
                </div>


                <!-- /. ROW  -->
                <form action="manage_owner_userprofile_updatedata.php?id=<?php $pecah['id']?>" method="POST">
                    <div class="form-row">
                        <div id="data">
                               
                            <div class="form-group col-md-6">
                                <label for="namalengkap">Nama Lengkap</label>
                                <input type="text" class="form-control" name="nama_lengkap" value="<?php echo $pecah['nama_lengkap'] ?>" id="namalengkap" placeholder="ex : Arya Dhika">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ttl">Tanggal Lahir</label>
                                <input type="date" class="form-control" min="1985-12-30" id="ttl" name="ttl" value="<?php echo $pecah['ttl'] ?>">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                <select  class="form-control" name="jk" id="jenis_kelamin" required>
                                    <option disabled="disabled" selected="selected">Pilih jenis kelamin...</option>
                                    <option  value="laki-laki" <?php if($pecah['jk'] == 'laki-laki')echo 'selected' ?>>laki-laki</option>
                                    <option value="wanita" <?php if($pecah['jk'] == 'wanita') echo 'selected' ?>>wanita</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="username">Username</label>
                                <input type="username" class="form-control" name="username"  value="<?php echo $pecah['username'] ?>" id="username">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="password">Password</label>
                                <input type="password" class="form-control"  value="<?php echo $pecah['password'] ?>" name="password" id="password">
                                <label>
                                    <input type="checkbox" name="" onclick="myfunction()"> Lihat Password
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6"><br> <button type="submit" name="update" class="btn btn-primary">Update</button></div>
                </form>
            </div>
            <!-- /. ROW  -->
            <footer>
                <div class="text-center w-full">
                    <small class="block"> copyright &copy; STAMINA GYM. All Rights reserved.</small>
                </div>
            </footer>
        </div>
    </div>

    <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- Bootstrap Js -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>


    <script src="assets/js/easypiechart.js"></script>
    <script src="assets/js/easypiechart-data.js"></script>


    <!-- Custom Js -->
    <script src="assets/js/custom-scripts.js"></script>


</body>
<script>
    function myfunction() {
        var pass = document.getElementById('password')
        if (pass.type == 'password') {
            pass.type = 'text';
        } else {
            pass.type = 'password';
        }
    }
</script>


</html>