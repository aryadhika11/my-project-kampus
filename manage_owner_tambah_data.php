<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php

include 'koneksi.php';
$sql = mysqli_query($koneksi, "SELECT * FROM admindnowner WHERE kategori = 'owner' ");
$dika = mysqli_fetch_assoc($sql);
?>


<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>STAMINA--Manage Admin</title>
    <!-- Bootstrap Styles-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="fh5co-logo">
                    <a class="navbar-brand" href="manage_admin.php"><strong>STAMINA <span>.</span> </strong></a></div>
            </div>

            <ul class="nav navbar-top-links navbar-right">

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="manage_owner_userprofile.php"><i class="fa fa-user fa-fw"></i> Nyonya <?php echo $dika['username'] ?> profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout.php" onclick="return confirm('Ingin Logout?')"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>

                </li>

            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div id="sideNav" href=""><i class="fa fa-caret-right"></i></div>
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li>
                        <a href="manage_owner.php"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>

                    <li>
                        <a class="active-menu" href="laporanowner.php"><i class="fa fa-file-o"></i> Laporan </a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">
                            Hallo Nyonya <?php echo $dika['username'] ?>!
                        </h1>

                    </div>
                </div>


                <!-- /. ROW  -->

                <div class="row">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <h2>Laporan</h2>
                            <br>
                            <h2 style="text-align: center;">Data Admin</h2>
                            <br>
                            <form action="" method="POST">

                                <button type="submit" value="update" name="update" class="btn btn-primary">Update</button>
                                <br>
                                <br>
                                <div class="form-row">

                                    <div id="data">

                                        <div class="form-group col-md-6">
                                            <label for="namalengkap">Nama Lengkap</label>
                                            <input type="text" class="form-control" name="nama_lengkap" id="namalengkap" placeholder="ex : Arya Dhika" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="ttl">Tanggal Lahir</label>
                                            <input type="date" min="1985-12-30" class="form-control" id="ttl" name="ttl" required>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="jenis_kelamin">Jenis Kelamin</label>
                                            <select class="form-control" name="jk" id="jenis_kelamin" required>
                                                <option disabled="disabled" selected="selected">Pilih jenis kelamin...</option>
                                                <option>laki-laki</option>
                                                <option>wanita</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="lama_kerja">Lama kerja</label>
                                            <input type="text" class="form-control" name="lama_kerja" id="lama_kerja" required placeholder="ex : 5 tahun">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="posisi">Posisi</label>
                                            <input type="text" class="form-control" name="posisi" id="posisi" required placeholder="ex : Kepala Divisi Admin">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="pk">Pengalaman Kerja</label>
                                            <input type="text" class="form-control" name="pengalaman_kerja" id="pk" required placeholder="ex : 1. Trainer Muda ALOHA GYM, 2tahun">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="username">Username</label>
                                            <input type="username" class="form-control" name="username" required id="username">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="kategori">Kategori</label>
                                            <select class="form-control" name="kategori" id="kategori" required>
                                                <option disabled="disabled" selected="selected">Pilih Kategori...</option>
                                                <option value="admin">admin</option>
                                                <option value="owner">owner</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" maxlength="8" value="password" required name="password" id="password">
                                            <label>
                                                <input type="checkbox" name="" onclick="myfunction()"> Lihat Password
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </form>
                            <?php
                            include "koneksi.php";
                            if (isset($_POST['update'])) {
                                $username = $_POST['username'];
                                $cek = mysqli_query($koneksi, "SELECT * FROM admindnowner WHERE id");
                                $ambil = mysqli_fetch_assoc($cek);
                                $nama = $_POST['nama_lengkap'];
                                $ttl = $_POST['ttl'];
                                $jk = $_POST['jk'];
                                $lamakerja = $_POST['lama_kerja'];
                                $posisi = $_POST['posisi'];
                                $peng_ker = $_POST['pengalaman_kerja'];
                                $username = $_POST['username'];
                                $kategori = $_POST['kategori'];
                                $pass = $_POST['password'];
                                if ($username != $ambil['username']) {
                                //    $input = mysqli_query($koneksi, "INSERT INTO admindnowner (`id`, `nama_lengkap`, `ttl`, `jk`, `lama_kerja`, `posisi`, `pengalaman_kerja`, `username`, `password`, `kategori`) VALUES (id, '$nama', '$ttl', '$jk', '$lamakerja', '$posisi', '$peng_ker', '$username', '$pass', '$kategori')");
                                    // $input = mysqli_query($koneksi, "INSERT INTO admindnowner nama_lengkap = '$nama', ttl = '$ttl',  jk = '$jk', lama_kerja = '$lamakerja', posisi = '$posisi', pengalaman_kerja = '$peng_ker', username = '$username', kategori = '$kategori', password = '$pass'  ");
                                       $input = mysqli_query($koneksi, "INSERT INTO admindnowner VALUES(id, '".$_POST['nama_lengkap']."', '".$_POST['ttl']."', '".$_POST['jk']."', '".$_POST['lama_kerja']."', '".$_POST['posisi']."', '".$_POST['pengalaman_kerja']."', '".$_POST['username']."', '".$_POST['kategori']."', '".$_POST['password']."')");
                                    if ($input) {
                                        echo '<script>alert("Data Berhasil ditambahkan");document.location="laporanowner.php";</script>';
                                    } else {
                                        echo '' . mysqli_error($koneksi);
                                    }
                                } else {
                                    echo '<script>alert("Maaf, Username telah digunakan");document.location="manage_owner_tambah_data_user.php";</script>';
                                }
                            }



                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function myfunction() {
                var pass = document.getElementById('password')
                if (pass.type == 'password') {
                    pass.type = 'text';
                } else {
                    pass.type = 'password';
                }
            }
        </script>
        <!-- /. ROW  -->
        <footer>
            <div class="text-center w-full">
                <small class="block"> copyright &copy; STAMINA GYM. All Rights reserved.</small>
            </div>
        </footer>
    </div>
    <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- Bootstrap Js -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>


    <script src="assets/js/easypiechart.js"></script>
    <script src="assets/js/easypiechart-data.js"></script>


    <!-- Custom Js -->
    <script src="assets/js/custom-scripts.js"></script>


</body>

</html>