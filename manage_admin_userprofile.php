﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<?php

include "koneksi.php";

session_start();
$username = $_SESSION['username'];
$cek_user = mysqli_query($koneksi, "SELECT * FROM admindnowner WHERE username = '$username' ");
$pecah = mysqli_fetch_assoc($cek_user);
?>



?>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>STAMINA--Manage Admin</title>
    <!-- Bootstrap Styles-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div id="fh5co-logo">
                    <a class="navbar-brand" href="manage_admin.php"><strong>STAMINA <span>.</span> </strong></a></div>
            </div>

            <ul class="nav navbar-top-links navbar-right">

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> <?php echo $pecah['username'] ?> Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>

                </li>

            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div id="sideNav" href=""><i class="fa fa-caret-right"></i></div>
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                    <li>
                        <a href="manage_admin.php"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>

                    <li>
                        <a href="kotakmasuk.php"><i class="fa fa-envelope-o"></i>Kotak Masuk</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-refresh"></i>Update Data<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">Informasi Kesehatan</a>
                            </li>
                            <li>
                                <a href="#">Video Latihan</a>
                            </li>
                            <li>
                                <a href="#">Kelas</a>
                            </li>
                            <li>
                                <a href="#">promosi</a>
                            </li>
                            <li>
                                <a href="#">Keanggotaan</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="penjualan.php"><i class="fa fa-shopping-cart"></i> Penjualan</a>
                    </li>
                    <li>
                        <a href="laporanadmin.php"><i class="fa fa-file-o"></i> Laporan </a>
                    </li>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">


                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">

                            Profile <?php echo $pecah['username'] ?>
                        </h1>
                    </div>
                </div>


                <!-- /. ROW  -->
                <form action="manage_admin_userprofile_updatedata.php?id=<?php echo $pecah['id']?>" method="post">
                    <div class="form-row">
                        
                        <div id="data">
                               
                            <div class="form-group col-md-6">
                                <label for="namalengkap">Nama Lengkap</label>
                                <input type="text" class="form-control" name="nama_lengkap" value="<?php echo $pecah['nama_lengkap'] ?>" id="namalengkap" placeholder="ex : Arya Dhika">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ttl">Tanggal Lahir</label>
                                <input type="date" class="form-control" id="ttl" name="ttl" value="<?php echo $pecah['ttl'] ?>">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                <select  class="form-control" name="jk" id="jenis_kelamin" required>
                                    <option disabled="disabled" selected="selected">Pilih jenis kelamin...</option>
                                    <option  value="laki-laki" <?php if($pecah['jk'] == 'laki-laki')echo 'selected' ?>>laki-laki</option>
                                    <option value="wanita" <?php if($pecah['jk'] == 'wanita') echo 'selected' ?>>wanita</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="lama_kerja">Lama kerja</label>
                                <input type="text" class="form-control" name="lama_kerja" id="lama_kerja"  value="<?php echo $pecah['lama_kerja'] ?>" placeholder="ex : 5 tahun">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="posisi">Posisi</label>
                                <input type="text" class="form-control" name="posisi" id="posisi"  value="<?php echo $pecah['posisi'] ?>" placeholder="ex : Kepala Divisi Admin">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="pk">Pengalaman Kerja</label>
                                <input type="text" class="form-control" name="pengalaman_kerja" id="pk" value="<?php echo $pecah['pengalaman_kerja'] ?>" placeholder="ex : 1. Trainer Muda ALOHA GYM, 2tahun">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="username">Username</label>
                                <input type="username" class="form-control" name="username"  value="<?php echo $pecah['username'] ?>" id="username">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="password">Password</label>
                                <input type="password" class="form-control"  value="<?php echo $pecah['password'] ?>" name="password" id="password">
                                <label>
                                    <input type="checkbox" name="" onclick="myfunction()"> Lihat Password
                                </label>
                            </div>
                            
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <button type="submit" name="update" class="btn btn-primary">Update</button></div>
                </form>
            </div>
            <!-- /. ROW  -->
            <footer>
                <div class="text-center w-full">
                    <small class="block"> copyright &copy; STAMINA GYM. All Rights reserved.</small>
                </div>
            </footer>
        </div>
    </div>

    <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- Bootstrap Js -->
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Metis Menu Js -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>


    <script src="assets/js/easypiechart.js"></script>
    <script src="assets/js/easypiechart-data.js"></script>


    <!-- Custom Js -->
    <script src="assets/js/custom-scripts.js"></script>


</body>
<script>
    function myfunction() {
        var pass = document.getElementById('password')
        if (pass.type == 'password') {
            pass.type = 'text';
        } else {
            pass.type = 'password';
        }
    }
</script>


</html>